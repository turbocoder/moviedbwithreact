import * as Type from "../action/ActionTypes";
import initialState from "../InitialState";

export default function(state = initialState.favorites, action) {
  switch (action.type) {
    case Type.LOAD_MOVIES_FROM_DB_SUCCESS:
      return {
        ...state,
        list: action.list,
        orderBy: action.orderBy,
        loading: false
      };
    case Type.LOAD_MOVIE_FROM_CACHE_SUCCESS:
      return {
        ...state,
        selectedMovie: action.selectedMovie
      };
    case Type.ADD_MOVIE_TO_FAVORITES_SUCCESS:
      return {
        ...state,
        list: state.list.concat([action.movie])
      };
    case Type.REMOVE_MOVIE_FROM_FAVORITES_SUCCESS:
      let indexToRemove = state.list.findIndex(m => m.id === action.movie.id);
      return {
        ...state,
        list: state.list.filter((movie, index) => index != indexToRemove)
      };
    case Type.EMPTY_MOVIES_FROM_FAVORITES_LIST:
      return {
        ...state,
        list: [],
        orderBy: action.orderBy,
        loading: true
      };
    default:
      return state;
  }
}
