import * as Type from "../action/ActionTypes";
import initialState from "../InitialState";

export default function(state = initialState.movies, action) {
  switch (action.type) {
    case Type.LOAD_MOVIES_FROM_API_SUCCESS:
      return {
        ...state,
        list: action.list,
        page: action.page,
        orderBy: action.orderBy,
        loading: false
      };
    case Type.LOAD_MORE_MOVIES_FROM_API_SUCCESS:
      return {
        ...state,
        list: state.list.concat(action.list),
        page: action.page
      };
    case Type.EMPTY_MOVIES_FROM_API:
      return {
        ...state,
        list: [],
        orderBy: action.orderBy,
        page: 1,
        loading: true
      }
    case Type.LOAD_MOVIE_FROM_CACHE_SUCCESS:
      return {
        ...state,
        selectedMovie: action.selectedMovie
      }
    default:
      return state;
  }
}
