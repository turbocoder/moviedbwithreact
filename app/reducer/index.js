import { combineReducers } from "redux";
import movies from "./MovieReducer";
import favorites from "./FavoriteReducer";

const reducers = combineReducers({
  movies,
  favorites
});

module.exports = reducers;
