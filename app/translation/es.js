module.exports = {
  add_favorite: "Agregar a favoritos",
  remove_favorite: "Remover de favoritos",
  movies: "Películas",
  favorites: "Favoritos",
  title: "Titulo",
  date: "Fecha",
  rating: "Puntuacion",
  no_movie_found: "No se encontro la película"
};
