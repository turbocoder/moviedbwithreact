import en from "./en";
import es from "./es";

module.exports = {
  en,
  es
};
