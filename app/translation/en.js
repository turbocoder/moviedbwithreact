module.exports = {
  add_favorite: "Add to favorites",
  remove_favorite: "Remove from favorites",
  movies: "Movies",
  favorites: "Favorites",
  title: "Title",
  date: "Date",
  rating: "Rating",
  no_movie_found: "No movie found"
};
