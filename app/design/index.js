import Color from "./Color";
import Style from "./Style";

module.exports = {
  Color,
  Style
};
