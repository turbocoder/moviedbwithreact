import { StyleSheet } from "react-native";
import Color from "./Color";

const style = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: Color.primary
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10,
    color: Color.accent
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  },
  list: {
    marginBottom: 4,
    marginTop: 4
  },
  movie_item_container: {
    flex: 1,
    margin: 10,
    backgroundColor: Color.primary
  },
  movie_detail_header: {
    color: Color.primary
  },
  movie_detail_overview: {
    color: Color.primary
  },
  button_container: {
    flexDirection: "row"
  },
  filter_button: {
    flex: 0.33
  },
  movie_detail_view: {
    backgroundColor: "#ffffff"
  }
});

module.exports = style;
