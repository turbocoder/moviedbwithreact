import React, { PureComponent } from "react";
import { TouchableHighlight } from "react-native";
import { View, Button, Icon, Text } from "@shoutem/ui";
import { Style } from "../design";
import Config from "../Config";
import { DataService, TranslationService } from "../service";

export default class MovieFilter extends PureComponent {
  render() {
    const { activeFilter, onItemClickListener } = this.props;
    return (
      <View styleName="horizontal h-start space-between">
        <Button
          styleName={
            activeFilter === "title.asc" ? "full-width" : "full-width secondary"
          }
          onPress={() => {
            onItemClickListener("title.asc");
          }}
        >
          <Icon name="page" />
          <Text>{TranslationService.t("title")}</Text>
        </Button>
        <Button
          styleName={
            activeFilter === "release_date.asc"
              ? "full-width"
              : "full-width secondary"
          }
          onPress={() => {
            onItemClickListener("release_date.asc");
          }}
        >
          <Icon name="rsvp" />
          <Text>{TranslationService.t("date")}</Text>
        </Button>
        <Button
          styleName={
            activeFilter === "vote_average.desc"
              ? "full-width"
              : "full-width secondary"
          }
          onPress={() => {
            onItemClickListener("vote_average.desc");
          }}
        >
          <Icon name="add-to-favorites-on" />
          <Text>{TranslationService.t("rating")}</Text>
        </Button>
      </View>
    );
  }
}
