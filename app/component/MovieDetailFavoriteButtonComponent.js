import React, { PureComponent } from "react";
import {
  TouchableHighlight,
  NativeEventEmitter,
  DeviceEventEmitter
} from "react-native";
import { Button, Icon, Text } from "@shoutem/ui";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Style } from "../design";
import Config from "../Config";
import { DataService, TranslationService } from "../service";
import { FavoriteActions } from "../action";

class MovieDetailFavoriteButton extends PureComponent {
  
  addFavorite(movie) {
    this.props.action.addMovieToFavorites(this.props.movie);
  }

  removeFavorite(movie) {
    this.props.action.removeMovieFromFavorites(this.props.movie);
  }

  render() {
    if (this.props.isFavorite) {
      return (
        <Button
          onPress={() => {
            this.removeFavorite(this.props.movie);
          }}
        >
          <Icon name="add-to-favorites-on" />
          <Text>{TranslationService.t("remove_favorite")}</Text>
        </Button>
      );
    } else {
      return (
        <Button
          onPress={() => {
            this.addFavorite(this.props.movie);
          }}
        >
          <Icon name="add-to-favorites-off" />
          <Text>{TranslationService.t("add_favorite")}</Text>
        </Button>
      );
    }
  }
}

function mapStateToProps(state, ownProps) {
  return {
    isFavorite: state.favorites.list.filter(m => m.id === ownProps.movie.id).length
  };
}

function mapDispatchToProps(dispatch) {
  return {
    action: bindActionCreators(FavoriteActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MovieDetailFavoriteButton);
