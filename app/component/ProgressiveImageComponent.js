import React, { PureComponent } from "react";
import { Image } from "@shoutem/ui";

export default class ProgressiveImage extends PureComponent {
  state = { showDefault: true, error: false };

  render() {
    var image = this.state.showDefault
      ? this.props.defaultSource
      : this.state.error
        ? this.props.defaultSource
        : this.props.source;

    return (
      <Image
        styleName={this.props.styleName}
        source={image}
        onLoadEnd={() => this.setState({ showDefault: false })}
        onError={() => this.setState({ error: true })}
        resizeMode={this.props.resizeMode}
      />
    );
  }
}
