import React, { PureComponent } from "react";
import { Text, View, FlatList } from "react-native";
import MovieItem from "./MovieItemComponent";
import { Style } from "../design";

export default class MovieList extends PureComponent {
  render() {
    const { movies, onEndReachedListener, onItemClickListener } = this.props;
    if (!movies || movies.length == 0)
      return (
        <View style={Style.container}>
          <Text style={Style.welcome}>Lista vacia</Text>
        </View>
      );

    return (
      <View style={Style.container}>
        <FlatList
          style={Style.list}
          data={movies}
          numColumns={2}
          renderItem={element => (
            <MovieItem
              movie={element.item}
              onItemClickListener={onItemClickListener}
            />
          )}
          onEndReached={onEndReachedListener}
        />
      </View>
    );
  }
}
