import React, { PureComponent } from "react";
import { TouchableHighlight } from "react-native";
import { View, Image, Card, Subtitle } from "@shoutem/ui";
import { Style } from "../design";
import Config from "../Config";
import { DataService } from "../service";
import ProgressiveImage from "./ProgressiveImageComponent";
import MovieDetailFavoriteButton from "./MovieDetailFavoriteButtonComponent";

export default class MovieItem extends PureComponent {

  render() {
    const { movie, onItemClickListener } = this.props;
    return (
      <View style={{ margin: 4 }}>
        <Card>
          <TouchableHighlight
            onPress={() => {
              onItemClickListener(movie.id);
            }}
          >
            <ProgressiveImage
              styleName="medium-wide"
              source={{ uri: Config.CDN_URL + movie.poster_path }}
              defaultSource={require("../res/placeholder.png")}
            />
          </TouchableHighlight>
          <View styleName="content">
            <TouchableHighlight
              onPress={() => {
                onItemClickListener(movie.id);
              }}
            >
              <Subtitle>{movie.title}</Subtitle>
            </TouchableHighlight>
            <MovieDetailFavoriteButton movie={movie} />
          </View>
        </Card>
      </View>
    );
  }
}
