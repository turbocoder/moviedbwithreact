module.exports = {
  orderBy: (list, order, desc) => {
    let array = [...list];
    var done = false;
    while (!done) {
      done = true;
      for (var i = 1; i < array.length; i += 1) {
        if (
          desc ? array[i - 1][order] < array[i][order] : array[i - 1][order] > array[i][order]
        ) {
          done = false;
          var tmp = array[i - 1];
          array[i - 1] = array[i];
          array[i] = tmp;
        }
      }
    }
    return array;
  }
};
