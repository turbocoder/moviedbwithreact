import React, { PureComponent } from "react";
import { Text, View, Image, Button } from "react-native";
import { Spinner } from "@shoutem/ui";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { MovieList, MovieFilter } from "../component";
import { ApiService, DataService } from "../service";
import { Style, Color } from "../design";
import { MovieActions } from "../action";

class MovieListView extends PureComponent {
  componentDidMount() {
    this.props.action.loadMoviesFromApi(this.props.page, this.props.orderBy);
  }

  onEndReachedListener() {
    this.props.action.loadMoreMoviesFromApi(
      this.props.page,
      this.props.orderBy
    );
  }

  onItemClickListener(id) {
    this.props.action.loadMovieFromCache(id).then(movie => {
      this.props.navigator.showModal({
        screen: "view.MovieDetail",
        title: movie.title,
        navigatorStyle: {
          navBarNoBorder: true,
          statusBarTextColorScheme: "light",
          navBarBackgroundColor: Color.primary,
          navBarTextColor: Color.accent,
          topBarElevationShadowEnabled: false,
          statusBarColor: Color.primary_dark,
          navBarButtonColor: Color.accent
        }
      });
    });
  }

  filterBy(orderBy) {
    this.props.action.emptyMoviesFromApi(orderBy);
    this.props.action.loadMoviesFromApi(this.props.page, orderBy);
  }

  render() {
    if (this.props.loading)
      return (
        <View style={Style.container}>
          <MovieFilter
            activeFilter={this.props.orderBy}
            onItemClickListener={filter => {
              this.filterBy(filter);
            }}
          />
          <View style={Style.container}>
            <Spinner size="large" color={Color.accent} />
          </View>
        </View>
      );

    return (
      <View style={Style.container}>
        <MovieFilter
          activeFilter={this.props.orderBy}
          onItemClickListener={filter => {
            this.filterBy(filter);
          }}
        />
        <MovieList
          movies={this.props.movies}
          onEndReachedListener={() => {
            this.onEndReachedListener();
          }}
          onItemClickListener={id => {
            this.onItemClickListener(id);
          }}
        />
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    movies: state.movies.list,
    loading: state.movies.loading,
    page: state.movies.page,
    orderBy: state.movies.orderBy
  };
}

function mapDispatchToProps(dispatch) {
  return {
    action: bindActionCreators(MovieActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MovieListView);
