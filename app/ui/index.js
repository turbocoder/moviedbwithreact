import FavoriteList from "./FavoriteListView";
import MovieDetail from "./MovieDetailView";
import MovieList from "./MovieListView";

module.exports = {
  FavoriteList,
  MovieDetail,
  MovieList
};
