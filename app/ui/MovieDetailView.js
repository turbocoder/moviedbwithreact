import React, { PureComponent } from "react";
import { ScrollView } from "react-native";
import {
  MovieList,
  MovieDetailFavoriteButton,
  ProgressiveImage
} from "../component";
import {
  View,
  NavigationBar,
  Title,
  Text,
  Subtitle,
  Tile,
  Overlay,
  Heading,
  Button,
  Icon,
  Row
} from "@shoutem/ui";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Config from "../Config";
import { Style, Color } from "../design";
import { DataService, TranslationService } from "../service";
import { MovieActions } from "../action";

class MovieDetail extends PureComponent {
  render() {
    if (!this.props.movie)
      return (
        <View overlay={Color.primary}>
          <Text style={Style.welcome}>
            {TranslationService.t("no_movie_found")}
          </Text>
        </View>
      );

    return (
      <View
        styleName="fill-parent overlay"
        style={{
          backgroundColor: Color.md_white_1000
        }}
      >
        <ScrollView>
          <View styleName="vertical h-center space-between">
            <ProgressiveImage
              styleName="large"
              source={{ uri: Config.CDN_URL + this.props.movie.poster_path }}
              defaultSource={require("../res/placeholder.png")}
            />
            <Row>
              <View styleName="vertical h-start space-between">
                <Title>{this.props.movie.title}</Title>
                <Subtitle>{this.props.movie.overview}</Subtitle>
              </View>
            </Row>

            <Row>
              <View styleName="horizontal h-center space-between">
                <Text>{this.props.movie.release_date}</Text>
                <View styleName="horizontal space-between">
                  <Icon name="add-to-favorites-on" />
                  <Text>{this.props.movie.vote_average}</Text>
                </View>
              </View>
            </Row>
            <MovieDetailFavoriteButton movie={this.props.movie} />
          </View>
        </ScrollView>
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    movie: state.movies.selectedMovie
  };
}

function mapDispatchToProps(dispatch) {
  return {
    action: bindActionCreators(MovieActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MovieDetail);
