import React, { PureComponent } from "react";
import { Text, View, Image, DeviceEventEmitter } from "react-native";
import { Spinner } from "@shoutem/ui";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { MovieFilter, MovieList } from "../component";
import { ApiService, DataService } from "../service";
import { Style, Color } from "../design";
import { FavoriteActions } from "../action";

class FavoriteListView extends PureComponent {

  componentDidMount() {
    this.props.action.loadMoviesFromDB(this.props.orderBy);
  }

  onItemClickListener(id) {
    this.props.action.loadMovieFromCache(id).then(movie => {
      this.props.navigator.showModal({
        screen: "view.MovieDetail",
        title: movie.title,
        passProps: { id: id },
        navigatorStyle: {
          navBarNoBorder: true,
          statusBarTextColorScheme: "light",
          navBarBackgroundColor: Color.primary,
          navBarTextColor: Color.accent,
          topBarElevationShadowEnabled: false,
          statusBarColor: Color.primary_dark,
          navBarButtonColor: Color.accent
        }
      });
    });
  }

  filterBy(orderBy) {
    this.props.action.emptyMoviesFromFavoritesList(orderBy);
    this.props.action.loadMoviesFromDB(orderBy);
  }

  render() {
    if (this.props.loading)
      return (
        <View style={Style.container}>
          <MovieFilter
            onItemClickListener={filter => {
              this.filterBy(filter);
            }}
          />
          <View style={Style.container}>
            <Spinner size="large" color={Color.accent} />
          </View>
        </View>
      );
    return (
      <View style={Style.container}>
        <MovieFilter
          activeFilter={this.props.orderBy}
          onItemClickListener={filter => {
            this.filterBy(filter);
          }}
        />
        <MovieList
          movies={this.props.movies}
          onItemClickListener={id => {
            this.onItemClickListener(id);
          }}
        />
      </View>
    );
  }
}

function mapStateToProps(state, ownProps) {
  return {
    movies: state.favorites.list,
    loading: state.favorites.loading,
    orderBy: state.favorites.orderBy
  };
}

function mapDispatchToProps(dispatch) {
  return {
    action: bindActionCreators(FavoriteActions, dispatch)
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(FavoriteListView);
