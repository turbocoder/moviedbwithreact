import { create } from "apisauce";
import cache from "memory-cache";
import PouchDB from "pouchdb-react-native";

const database = new PouchDB("MovieDB");

module.exports = {
  cache: cache,
  database: database
};
