import I18n from "react-native-i18n";
import translation from "../translation";

I18n.fallbacks = true;
I18n.translations = translation;

module.exports = {
  t: input => I18n.t(input)
};
