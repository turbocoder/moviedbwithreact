import { create } from "apisauce";
import Config from "../Config";

// define the api
const api = create({
  baseURL: Config.API_URL
});

module.exports = {
  getMovieList: function(sortBy, page) {
    if (!sortBy || !page) return;

    return new Promise((resolve, reject) => {
      api
        .get(
          `discover/movie?sort_by=${sortBy}&page=${page}&api_key=${
            Config.API_KEY
          }`
        )
        .then(response => {
          if (!response) return reject("no response");
          if (!response.data) return reject("empty data");

          resolve(response.data.results);
        })
        .catch(reject);
    });
  }
};
