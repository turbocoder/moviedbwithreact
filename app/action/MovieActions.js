import * as Type from "./ActionTypes";
import { ApiService, DataService } from "../service";

export function loadMoviesFromApiSuccess(movies, page, orderBy) {
  return {
    type: Type.LOAD_MOVIES_FROM_API_SUCCESS,
    list: movies,
    orderBy: orderBy,
    page: page
  };
}

export function loadMoviesFromApi(page, orderBy) {
  return function(dispatch) {
    return ApiService.getMovieList(orderBy, page)
      .then(movies => {
        movies.forEach(movie => {
          DataService.cache.put(movie.id, movie);
        });
        return movies;
      })
      .then(movies => {
        dispatch(loadMoviesFromApiSuccess(movies, ++page, orderBy));
      })
      .catch(console.log);
  };
}

export function loadMoreMoviesFromApiSuccess(movies, page, orderBy) {
  return {
    type: Type.LOAD_MORE_MOVIES_FROM_API_SUCCESS,
    list: movies,
    page: page
  };
}

export function loadMoreMoviesFromApi(page, orderBy) {
  return function(dispatch) {
    return ApiService.getMovieList(orderBy, page)
      .then(movies => {
        movies.forEach(movie => {
          DataService.cache.put(movie.id, movie);
        });
        return movies;
        })
      .then(movies => {
          dispatch(loadMoreMoviesFromApiSuccess(movies, ++page, orderBy));
        })
      .catch(console.log);
  };
}

export function emptyMoviesFromApi(orderBy) {
  return {
    type: Type.EMPTY_MOVIES_FROM_API,
    orderBy: orderBy
  };
}

export function loadMovieFromCacheSuccess(movie) {
  return {
    type: Type.LOAD_MOVIE_FROM_CACHE_SUCCESS,
    selectedMovie: movie
  };
}

export function loadMovieFromCache(id) {
  return function(dispatch) {
    return new Promise((resolve, reject) => {
      let movie = DataService.cache.get(id);
      if (!movie) return reject("movie not found");
      resolve(movie);
    })
      .then(movie => {
        dispatch(loadMovieFromCacheSuccess(movie));
        return movie;
      })
      .catch(console.log);
  };
}
