import * as MovieActions from "./MovieActions";
import * as FavoriteActions from "./FavoriteActions";

module.exports = {
  MovieActions,
  FavoriteActions
};
