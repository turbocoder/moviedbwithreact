import * as Type from "./ActionTypes";
import { DataService } from "../service";
import Tools from "../Tools";

export function loadMoviesFromDBSuccess(movies, orderBy) {
  return {
    type: Type.LOAD_MOVIES_FROM_DB_SUCCESS,
    list: movies,
    orderBy: orderBy
  };
}

export function loadMoviesFromDB(orderBy) {
  return function(dispatch) {
    let order = orderBy.split(".")[0];
    let desc = orderBy.split(".")[1] === "desc";

    return DataService.database
      .get("favorites")
      .then(doc => Object.keys(doc.content).map(key => doc.content[key]))
      .then(movies => Tools.orderBy(movies, order, desc))
      .then(movies => {
        movies.forEach(movie => {
          DataService.cache.put(movie.id, movie);
        });
        return movies;
      })
      .then(movies => dispatch(loadMoviesFromDBSuccess(movies, orderBy)))
      .catch(err => {
        if (err.status === 404) {
          DataService.database.put({ _id: "favorites", content: {} });
        } else {
          console.log(err);
        }
        dispatch(loadMoviesFromDBSuccess([], orderBy));
      });
  };
}

export function loadMovieFromCacheSuccess(movie) {
  return {
    type: Type.LOAD_MOVIE_FROM_CACHE_SUCCESS,
    selectedMovie: movie
  };
}

export function loadMovieFromCache(id) {
  return function(dispatch) {
    return new Promise((resolve, reject) => {
      let movie = DataService.cache.get(id);
      if (!movie) return reject("movie not found");
      resolve(movie);
    })
      .then(movie => {
        dispatch(loadMovieFromCacheSuccess(movie));
        return movie;
      })
      .catch(console.log);
  };
}

export function addMovieToFavoritesSuccess(movie) {
  return {
    type: Type.ADD_MOVIE_TO_FAVORITES_SUCCESS,
    movie: movie
  };
}

export function addMovieToFavorites(movie) {
  return function(dispatch) {
    return DataService.database
      .get("favorites")
      .then(doc => {
        doc.content[movie.id] = movie;
        return DataService.database.put(doc);
      })
      .then(() => {
        dispatch(addMovieToFavoritesSuccess(movie));
      })
      .catch(console.log);
  };
}

export function removeMovieFromFavoritesSuccess(movie) {
  return {
    type: Type.REMOVE_MOVIE_FROM_FAVORITES_SUCCESS,
    movie: movie
  };
}

export function removeMovieFromFavorites(movie) {
  return function(dispatch) {
    return DataService.database
      .get("favorites")
      .then(doc => {
        delete doc.content[movie.id];
        return DataService.database.put(doc);
      })
      .then(() => {
        dispatch(removeMovieFromFavoritesSuccess(movie));
      })
      .catch(console.log);
  };
}

export function emptyMoviesFromFavoritesList(orderBy) {
  return {
    type: Type.EMPTY_MOVIES_FROM_FAVORITES_LIST,
    orderBy: orderBy
  };
}
