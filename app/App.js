import { Navigation } from "react-native-navigation";
import { Provider } from "react-redux";
import Store from "./Store";
import { FavoriteList, MovieDetail, MovieList } from "./ui";
import { Color } from "./design";
import { TranslationService } from "./service";
import { displayName } from "../app.json";

Navigation.registerComponent(
  "view.MovieList",
  () => MovieList,
  Store,
  Provider
);
Navigation.registerComponent(
  "view.FavoriteList",
  () => FavoriteList,
  Store,
  Provider
);
Navigation.registerComponent(
  "view.MovieDetail",
  () => MovieDetail,
  Store,
  Provider
);

Navigation.startTabBasedApp({
  tabs: [
    {
      label: TranslationService.t("movies"),
      screen: "view.MovieList",
      title: displayName,
      icon: require("./res/ic_movie.png"),
      navigatorStyle: {
        navBarNoBorder: true,
        statusBarTextColorScheme: "light",
        navBarBackgroundColor: Color.primary,
        navBarTextColor: Color.accent,
        topBarElevationShadowEnabled: false,
        statusBarColor: Color.primary_dark
      }
    },
    {
      label: TranslationService.t("favorites"),
      screen: "view.FavoriteList",
      title: displayName,
      icon: require("./res/ic_star.png"),
      navigatorStyle: {
        navBarNoBorder: true,
        statusBarTextColorScheme: "light",
        navBarBackgroundColor: Color.primary,
        navBarTextColor: Color.accent,
        topBarElevationShadowEnabled: false,
        statusBarColor: Color.primary_dark
      }
    }
  ],
  tabsStyle: {
    tabBarSelectedButtonColor: Color.md_blue_400
  },
  appStyle: {
    orientation: "portrait"
  }
});
