module.exports = {
  movies: {
    list: [],
    loading: true,
    page: 1,
    orderBy: "release_date.asc",
    selectedMovie: {}
  },
  favorites: {
    list: [],
    loading: true,
    orderBy: "release_date.asc"
  }
};
