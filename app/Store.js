import { createStore, applyMiddleware } from "redux";
import thunk from "redux-thunk";
import logger from "redux-logger";
import reducers from "./reducer";
import initialState from "./InitialState";

let middleware = [thunk];

if (__DEV__) {
  const reduxImmutableStateInvariant = require("redux-immutable-state-invariant").default();
  middleware = [...middleware, reduxImmutableStateInvariant, logger];
} else {
  middleware = [...middleware];
}

const store = createStore(
  reducers,
  initialState,
  applyMiddleware(...middleware)
);

module.exports = store;
