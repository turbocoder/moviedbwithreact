# MovieDBWithReact

## Detalles

**Creador:** Yamil García Hernández

## Especificaciones

Hacer una aplicación que permita a los usuarios:

- Ver películas (deben ser cacheadas)
- Ver las películas marcadas como favoritas (deben ser persistidas localmente)

Ambos listados de películas deben ser presentados como grids, y deben poder ser organizados por los criterios listados a continuación:

- Nombre
- Año de publicación (predeterminado)
- Rating

La aplicación consistirá de dos pantallas, descritas a continuación:

**Pantalla Principal**

- Nombre de la aplicación
- Filtros
- Grid de películas (como un tab)
- Películas favoritas (como un tab)

**Pantalla de detalle**

- Detalle de película, a continuación la estructura de la misma
- Título de la película
- Sipnosis
- Cover de la película
- Rating
- Botón (debe ser un indicador al mismo tiempo) para agregar/remover del listado de favoritas.

**Conceptos a tomar en cuenta:**

- SOLID
- Clean Architecture

**Librearias recomendadas:**

- [react-navigation](https://github.com/wix/react-native-navigation)
- [react-redux](https://github.com/reduxjs/react-redux)
- [apisauce](https://github.com/infinitered/apisauce)
- [react-native-vector-icons](https://github.com/oblador/react-native-vector-icons)
- [react-native-i18n](https://github.com/AlexanderZaytsev/react-native-i18n)

**Fuente de datos:** [TheMovieDB.org](https://www.themoviedb.org/documentation/api)

### MIT License

Copyright (c) [2018] [Yamil Garcia Hernandez - MovieDB - MDB]

Permission is hereby granted, free of charge, to any person obtaining a copy
    of this software and associated documentation files (the "Software"), to deal
    in the Software without restriction, including without limitation the rights
    to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
    copies of the Software, and to permit persons to whom the Software is
    furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
    copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
    IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
    FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
    AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
    LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
    OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
    SOFTWARE.
